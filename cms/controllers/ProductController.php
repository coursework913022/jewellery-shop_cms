<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\Basket;
use models\Category;
use models\Product;
use models\User;

class ProductController extends \core\Controller
{
    public function indexAction()
    {

        $rows = Product::getProducts();
        $search = $_GET['search'];
        $filtered_rows = [];
        if($search!=null){
            for ($i = 0; $i <= count($rows); $i++){
                if(strpos($rows[$i]['name'] , $search) !== false){
                    $filtered_rows[] = $rows[$i];
                }
            }
        } else {
            $filtered_rows = $rows;
        }

        return $this->render(null, [
            'rows' => $filtered_rows
        ]);
    }

    public function addAction($params)
    {
        $category_id = intval($params[0]);
        if(empty($category_id)){
            $category_id = null;
        }

        $categories = Category::getCategories();
        if (!User::isAdmin()) {
            return $this->error(403);
        }
        if (Core::getInstance()->requestMethod == 'POST') {
            $errors = [];
            $_POST['name'] = trim($_POST['name']);
            if (empty($_POST['name'])) {
                $errors['name'] = 'Назва товару не вказана';
            }
            if (empty($_POST['category_id'])) {
                $errors['category_id'] = 'Категорія не вибрана';
            }
            if ($_POST['price'] <= 0) {
                $errors['price'] = 'Ціна некоректно задана';
            }
            if ($_POST['count'] <= 0) {
                $errors['count'] = 'Кількість товарів некоректно задана';
            }

            if (empty($errors)) {
                Product::addProduct($_POST);
                return $this->redirect('/product');
            } else {
                $model = $_POST;
                return $this->render(null, [
                    'errors' => $errors,
                    'model' => $model,
                    'categories' => $categories,
                    'category_id' => $category_id
                ]);
            }
        }
        return $this->render(null, [
            'categories' => $categories,
            'category_id' => $category_id
        ]);
    }

    public function editAction($params)
    {
        $id = intval($params[0]);

        if (!User::isAdmin()) {
            return $this->error(403);
        }
        if ($id > 0) {
            $product = Product::getProductById($id);
            $categories = Category::getCategories();

            if (Core::getInstance()->requestMethod === 'POST') {
                $errors = [];
                $_POST['name'] = trim($_POST['name']);
                if (empty($_POST['name'])) {
                    $errors['name'] = 'Назва категорії не вказана';
                }
                if (empty($_POST['category_id'])) {
                    $errors['category_id'] = 'Категорія не вибрана';
                }
                if ($_POST['price'] <= 0) {
                    $errors['price'] = 'Ціна некоректно задана';
                }
                if ($_POST['count'] <= 0) {
                    $errors['count'] = 'Кількість товарів некоректно задана';
                }

                if (empty($errors)) {
                    Product::updateProduct($id, [$_POST['name'], $_POST['category_id'], $_POST['price'], $_POST['count'], $_POST['short_description'], $_POST['description']]);
                    if (!empty($_FILES['file']['tmp_name'])) {
                        Category::changePhoto($id, $_FILES['file']['tmp_name']);
                    }
                    return $this->redirect('/product/index');
                } else {
                    $model = $_POST;
                    return $this->render(null, [
                        'errors' => $errors,
                        'model' => $model,
                        'product' => $product,
                        'categories' => $categories
                    ]);
                }
            }
            return $this->render(null, [
                'product' => $product,
                'categories' => $categories
            ]);
        } else {
            return $this->error(403);
        }
    }

    public function deleteAction($params)
    {
        if (User::isAdmin()) {
            $id = $params[0];
            $yes = boolval($params[1] === 'yes');

            $product = Product::getProductById($id);
            if ($product!= null) {
                if ($yes) {
                    Product::deleteProduct($product['id']);
                    $this->redirect('/product/');
                }
                return $this->render(null, ['product' => $product]);
            } else {
                $this->error(404);
            }
        } else {
            $this->error(403);
        }
    }

    public function viewAction($params)
    {
        if($_POST){
            $productID = $_POST['product-id'];
            $count = $_POST['count'];
            Basket::addProduct($productID, $count);
        }
        $id = intval($params[0]);
        $category_product = Product::getProductById($id);
        $products = Product::getProducts();
        return $this->render(null, [
            'category_product' => $category_product,
            'products' => $products
        ]);
    }

}