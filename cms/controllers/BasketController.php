<?php

namespace controllers;

use models\Basket;

class BasketController extends \core\Controller
{
    public function indexAction() {
        //Basket::addProduct(1, 7);
        $basket = Basket::getProductsInBasket();
        return $this->render(null, [
            'basket' => $basket
        ]);
    }

}