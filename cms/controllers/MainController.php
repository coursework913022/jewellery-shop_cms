<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\Category;
use models\Product;

class MainController extends Controller
{
    public function indexAction()
    {
        $products = Product::getProducts();
        $category = Category::getCategories();
        $viewPath = null;
        return $this->render($viewPath, [
            'products' => $products,
            'category' => $category
        ]);
    }

    public function errorAction($code)
    {
        switch ($code) {
            case 404: case 403:
                return $this->render('views/main/error.php');
                break;
        }
    }
}