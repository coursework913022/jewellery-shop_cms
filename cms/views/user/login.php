<?php
/** @var string|null $error */
/** @var array $model */

core\Core::getInstance()->pageParams['title'] = 'Вхід на сайт';
?>
<div class="w-100"
     style="background: url('../../images/acount_bg.png') no-repeat; background-size: cover;">
    <main class="form-signin m-auto" style="background: rgba(253,253,254,0.7)">
        <h1 class="h3 mb-3 fw-normal text-center pt-4">Вхід на сайт</h1>

        <form method="post" action="" class="pb-5">
            <div class="form-floating mb-2">
                <input type="text" name="login" id="login" class="form-control" value="<?= $model['login'] ?>">
                <label for="login">Логін</label>
            </div>
            <div class="form-floating mb-2">
                <input type="password" name="password" id="password" class="form-control"
                       value="<?= $model['password'] ?>">
                <label for="password">Пароль</label>
            </div>

            <?php if (!empty($error)) : ?>
                <div class="form-text error mb-1 text-danger">
                    <?= $error ?>
                </div>
            <?php endif; ?>

            <div class="checkbox mb-4">
                <label>
                    <input type="checkbox" value="remember-me">
                    Запам'ятати
                </label>
            </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit">Увійти</button>
        </form>
        <div class="reg align-items-center text-center pb-5 pt-0">
            <h5>Немає акаунта? <a href="/user/register" class="text-decoration-underline text-black">Зареєструватися</a></h5>

        </div>
    </main>
</div>