<?php
/** @var array $errors */
/** @var array $model */

core\Core::getInstance()->pageParams['title'] = 'Реєстрація на сайт';
?>
<div class="vw-100" style="background: url('../../images/acount_bg.png') center no-repeat; background-size: cover;">
    <main class="form-signin m-auto" style="background: rgba(253,253,254,0.7)">
        <h1 class="h3 mb-3 fw-normal text-center pt-4">Реєстрація нового користувача</h1>
        <form method="post" action="" class="pb-4">
            <div class="form-floating mb-2">
                <input type="text" name="login" id="login" class="form-control" value="<?= $model['login'] ?>"
                       aria-describedby="emailHelp">
                <?php if (!empty($errors['login'])): ?>
                    <div class="form-text error text-danger" id="emailHelp">
                        <?= $errors['login']; ?>
                    </div>
                <?php endif; ?>
                <label for="login">Логін</label>
            </div>

            <div class="form-floating mb-2">
                <input type="password" name="password" id="password" class="form-control"
                       value="<?= $model['password'] ?>" aria-describedby="passwordHelp">
                <label for="password">Пароль</label>
            </div>

            <div class="form-floating mb-2">
                <input type="password" name="password2" id="password2" class="form-control"
                       value="<?= $model['password'] ?>" aria-describedby="password2Help"/>
                <?php if (!empty($errors['password'])): ?>
                    <div class="form-text error text-danger" id="password2Help">
                        <?= $errors['password']; ?>
                    </div>
                <?php endif; ?>
                <label for="password2">Введіть пароль ще раз</label>
            </div>


            <div class="form-floating mb-2">
                <input type="text" name="lastname" id="lastname" class="form-control" value="<?= $model['lastname'] ?>"
                       aria-describedby="lastnameHelp"/>
                <?php if (!empty($errors['lastname'])): ?>
                    <div class="form-text error text-danger" id="lastnameHelp">
                        <?= $errors['lastname']; ?>
                    </div>
                <?php endif; ?>
                <label for="lastname">Введіть прізвище: </label>
            </div>


            <div class="form-floating mb-2">
                <input type="text" name="firstname" id="firstname" class="form-control"
                       value="<?= $model['firstname'] ?>" aria-describedby="firstnameHelp"/>
                <?php if (!empty($errors['firstname'])): ?>
                    <div class="form-text error text-danger" id="firstnameHelp">
                        <?= $errors['firstname']; ?>
                    </div>
                <?php endif; ?>
                <label for="firstname">Введіть ім'я: </label>
            </div>

            <button class="mb-4 mt-3 w-100 btn btn-lg btn-primary" type="submit">Зареєструватися</button>
        </form>
    </main>
</div>

