<?php
/** @var array $products */

/** @var array $category */

use models\User;

?>

<!--category-->
<div class="container main-page-category mb-3" style="font-size: 1.2rem;">
    <div class="row m-4">
        <div class="col">
            <a href="/category/view/14" class="link-dark text-decoration-none">Каблучки</a>
        </div>
        <div class="col">
            <a href="/category/view/15" class="link-dark text-decoration-none">Сережки</a>
        </div>
        <div class="col">
            <a href="/category/view/16" class="link-dark text-decoration-none">Підвіски</a>
        </div>
        <div class="col">
            <a href="/category/view/233" class="link-dark text-decoration-none">Кольє</a>
        </div>
        <div class="col">
            <a href="/category/view/236" class="link-dark text-decoration-none">Браслети</a>
        </div>
    </div>
</div>
<!--/category-->

<!--slider-->
<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"
                aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
                aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"
                aria-label="Slide 3"></button>
        <!--<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>-->
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="../../images/mainPage_slide1.png" class="d-block w-100" alt="new-collection-image">
        </div>
        <div class="carousel-item">
            <img src="../../images/mainPage_slide2.png" class="d-block w-100" alt="new-collection-image">
        </div>
        <div class="carousel-item">
            <img src="../../images/mainPage_slide3.png" class="d-block w-100" alt="new-collection-image">
        </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
            data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
            data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>
<!-- /slider-->

<!-- category view(with pic) -->
<div class="row row-cols-1 row-cols-md-5 g-10 mt-3 categories-list mx-2">
    <?php foreach ($category as $row) : ?>
        <div class="col text-center mb-3">
            <a href="/category/view/<?= $row['id'] ?>" class="card-link">
                <div class="card">
                    <?php $filePath = 'files/category/' . $row['photo']; ?>
                    <?php if (is_file($filePath)): ?>
                        <img src="/<?= $filePath ?>" class="card-img-top row-cols-lg-4" alt="category-photo"
                             height="250">
                    <?php else : ?>
                        <img src="/static/images/default-image.jpg" class="card-img-top row-cols-lg-4"
                             alt="category-default-photo" height="250">
                    <?php endif; ?>
                    <div class="card-body">
                        <h5 class="card-title">
                            <?= $row['name'] ?>
                        </h5>
                    </div>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
</div>
<!-- /category view(with pic) -->

<!-- video -->
<video width="100%" muted autoplay loop preload="auto" class="m-0">
    <source src="images/home-bg.mp4">
</video>
<!-- /video -->

<!-- products view -->
<div class="row justify-content-around w-100 align-items-center mb-4 mt-4">
    <div class="container">
        <div class="row row-cols-1 row-cols-md-5 g-10 mx-1 categories-list">
            <?php foreach ($products as $product) : ?>
                <div class="col text-center mb-3">
                    <a href="/product/view/<?= $product['id'] ?>" class="card-link">
                        <div class="card" style="min-height: 100%;">
                            <?php $filePath = 'files/product/' . $product['photo']; ?>
                            <?php if (is_file($filePath)): ?>
                                <img src="/<?= $filePath ?>" class="card-img-top row-cols-lg-4" alt="product-photo"
                                     height="250">
                            <?php else : ?>
                                <img src="cms/static/images/default-image.jpg" class="card-img-top row-cols-lg-4"
                                     alt="category-default-photo" height="250">
                            <?php endif; ?>
                            <div class="card-body" style="text-align: left">
                                <div class="card-description">
                                    <h5>
                                        <p class="card-description__name"><?= $product['name']; ?></p>
                                    </h5>
                                    <h5>
                                        <p class="card-description__price"><?= $product['price'] ?> грн.</p>
                                    </h5>
                                    <div class="review">
                                        <i class="bi bi-bag">
                                            <svg width="24" height="24" fill="currentColor" class="bi bi-bag"
                                                 viewBox="0 0 16 17">
                                                <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z"/>
                                            </svg>
                                        </i>
                                        <span style="text-transform: uppercase; border-bottom: #c5811c 1px solid"> Переглянути</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>

    </div>
</div>
<!-- /products view -->





<!--//text-->
<!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci asperiores commodi dolor
    eaque eius et magnam magni mollitia nemo, nobis placeat possimus quaerat quidem quo quod tempora ut vel!
</p>-->

<!--//video-->
<!--<video width="100%" muted autoplay loop preload="auto" class="m-0">
    <source src="images/home-bg.mp4">
</video>-->

<!--//img-->
<!--<a href="/news" class="nav-link text-muted">
    <div class="w-100 mt-0">
        <img src="../../images/mainPage_slider1.png" class="d-block w-100" alt="NEW-COLLECTION">
    </div>
</a>-->