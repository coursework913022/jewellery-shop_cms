<?php

use core\Core;

Core::getInstance()->pageParams['title'] = 'Помилка 404';

?>

<div class="pb-3 pt-3 w-100 text-center align-items-center">
    <img class="mb-4" src="../../images/errorPage.png" alt="" height="350">
    <h2>
        Ми не змогли знайти те, що ви шукаєте.<br>
        Давайте знайдемо вам краще місце.
    </h2>
    <button type="button" class="btn btn-warning mt-4">
         <a href="/" style="text-decoration:none; color: #000">Повернутись на головну сторінку</a>
    </button>
</div>



