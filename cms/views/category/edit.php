<?php
/** @var array $category */

/** @var array $model */
/** @var array $errors */
?>
<div class="container">
    <div class="novelty w-100 h-25 text-center p-3">
        <h2>Редагування категорії</h2>
    </div>
    <div class="row row-cols-10 row-cols-md-3 mx-1 justify-content-around">
        <div class="col text-center mb-3">
            <form action="" method="post" enctype="multipart/form-data">
                <div class="mb-3">
                    <label for="name" class="form-label">Назва категорії</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder=""
                           value="<?= $category['name'] ?>">
                    <?php if (!empty($errors['name'])) : ?>
                        <div class="form-text error mb-4 text-danger">
                            <?= $errors['name'] ?>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="mb-3">
                    <label for="file" class="form-label">Оберіть нове зображення для категорії</label>
                    <div class="card mb-2">
                        <?php $filePath = 'files/category/' . $category['photo']; ?>
                        <?php if (is_file($filePath)): ?>
                            <img src="/<?= $filePath ?>" class="card-img-top row-cols-lg-4" alt="category-photo"
                                 height="300">
                        <?php else : ?>
                            <img src="/static/images/default-image.jpg" class="card-img-top row-cols-lg-4"
                                 alt="category-default-photo" height="300">
                        <?php endif; ?>
                    </div>
                    <input class="form-control form-control-lg" id="file" name="file" type="file"
                           accept="image/png">
                </div>

                <div class="mb-3">
                    <button class="btn btn-secondary btn-lg text-white text-decoration-none">Зберегти</button>
                </div>
            </form>
        </div>
    </div>
</div>

