<?php
/** @var array $model */
/** @var array $errors */
?>

<div class="container">
    <div class="novelty w-100 h-25 text-center p-3">
        <h2>Додавання категорії</h2>
    </div>
    <div class="row row-cols-10 row-cols-md-3 mx-1 justify-content-around">
        <div class="col text-center my-3">
            <form action="" method="post" enctype="multipart/form-data">
                <div class="mb-1">
                    <label for="name" class="form-label mb-1">Назва категорії</label>
                    <input type="text" class="form-control" id="name" name="name"
                           placeholder="Введіть назву категорії..">
                    <?php if (!empty($errors['name'])) : ?>
                        <div class="form-text error mb-4 text-danger">
                            <?= $errors['name'] ?>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="mb-4">
                    <label for="file" class="form-label mb-1">Оберіть файл з зображенням категорії</label>
                    <input class="form-control form-control-lg" id="file" name="file" type="file" accept="image/png">
                </div>

                <div>
                    <button class="btn btn-secondary btn-lg text-white text-decoration-none">Додати категорію</button>
                </div>
            </form>
        </div>
    </div>
</div>

