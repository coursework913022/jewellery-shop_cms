<?php
/** @var array $basket */

core\Core::getInstance()->pageParams['title'] = 'Кошик';
?>

<div class="row justify-content-around w-100 align-items-center mb-4" style="background: #f9f9fa">
    <div class="product-list col-4 my-lg-2 text-center">
        <h2>Кошик</h2>
    </div>
</div>
<div class="container">
    <table class="table">
        <tread>
            <tr>
                <th>№</th>
                <th>Назва товару</th>
                <th>Вартість одиниці</th>
                <th>Кількість</th>
                <th>Загальна вартість</th>
            </tr>
        </tread>
        <? $index = 1;
        foreach ($basket['products'] as $row) :?>
            <tr>
                <td><?= $index ?></td>
                <td><?= $row['product']['name'] ?></td>
                <td><?= $row['product']['price'] ?> грн.</td>
                <td><?= $row['count'] ?> шт.</td>
                <td><?= $row['product']['price'] * $row['count'] ?> грн.</td>
            </tr>
            <? $index++;
        endforeach; ?>
        <tfoot>
        <tr>
            <th colspan="4">Загальна сума:</th>
            <th><?= $basket['total_price'] ?> грн.</th>
        </tr>
        </tfoot>
    </table>
</div>
