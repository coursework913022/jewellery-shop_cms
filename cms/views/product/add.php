<?php
/** @var array $model */
/** @var array $errors */
/** @var array $categories */
/** @var int|null $category_id */
?>
<div class="colored-wrapper h-100 w-100" style="background-image: linear-gradient(to top, #cfd9df 0%, #e2ebf0 100%);">
    <div class="container">
        <div class="novelty w-100 h-25 text-center p-3">
            <h2>Додавання товару</h2>
        </div>
        <div class="row row-cols-10 row-cols-md-2 mx-1 justify-content-around py-3">
            <form action="" method="post" enctype="multipart/form-data">
                <!-- Назва товару -->
                <div class="mb-3 col text-center">
                    <label for="name" class="form-label mb-1">Назва товару</label>
                    <input type="text" class="form-control" id="name" name="name"
                           placeholder="Введіть назву товару..">
                    <?php if (!empty($errors['name'])) : ?>
                        <div class="form-text error text-danger">
                            <?= $errors['name'] ?>
                        </div>
                    <?php endif; ?>
                </div>

                <!-- Категорії -->
                <div class="mb-3 col text-center">
                    <label for="category_id" class="form-label mb-1">Оберіть категорію товару</label>
                    <select type="text" class="form-control" id="category_id" name="category_id">
                        <? foreach ($categories as $category): ?>
                            <option<? if($category['id'] == $category_id) echo 'selected'; ?>  value="<?= $category['id'] ?>"><?= $category['name'] ?></option>
                        <? endforeach; ?>
                    </select>
                    <?php if (!empty($errors['category_id'])) : ?>
                        <div class="form-text error text-danger">
                            <?= $errors['category_id'] ?>
                        </div>
                    <?php endif; ?>
                </div>

                <!-- Ціна -->
                <div class="mb-3 col text-center">
                    <label for="price" class="form-label mb-1">Ціна товару</label>
                    <input type="number" class="form-control" id="price" name="price"
                           placeholder="Введіть ціну..">
                    <?php if (!empty($errors['price'])) : ?>
                        <div class="form-text error text-danger">
                            <?= $errors['price'] ?>
                        </div>
                    <?php endif; ?>
                </div>

                <!-- Кількість -->
                <div class="mb-3 col text-center">
                    <label for="count" class="form-label mb-1">Кількість одиниць товару</label>
                    <input type="number" class="form-control" id="count" name="count"
                           placeholder="Введіть кількість товарів..">
                    <?php if (!empty($errors['count'])) : ?>
                        <div class="form-text error text-danger">
                            <?= $errors['count'] ?>
                        </div>
                    <?php endif; ?>
                </div>

                <!-- Короткий опис товару -->
                <div class="mb-3 col text-center">
                    <label for="short_description" class="form-label mb-1">Короткий опис товару</label>
                    <textarea type="text" class="ckeditor form-control" id="short_description" name="short_description"
                              placeholder="Введіть короткий опис..">
                    </textarea>
                    <?php if (!empty($errors['short_description'])) : ?>
                        <div class="form-text error text-danger">
                            <?= $errors['short_description'] ?>
                        </div>
                    <?php endif; ?>
                </div>

                <!-- розширений опис товару -->
                <div class="mb-3 col text-center">
                    <label for="description" class="form-label mb-1">Опис товару</label>
                    <textarea class="form-control ckeditor" id="description" name="description"
                              placeholder="Введіть опис.."
                              type="text"></textarea>
                    <?php if (empty($errors['description'])) : ?>
                        <div class="form-text error text-danger">
                            <?= $errors['description'] ?>
                        </div>
                    <?php endif; ?>
                </div>

                <!-- Чи відображати товар -->
                <div class="mb-3 col text-center">
                    <label for="visible" class="form-label mb-1">Чи відображати товар</label>
                    <select class="form-control" id="visible" name="visible">
                        <option value="1">Так</option>
                        <option value="0">Ні</option>
                    </select>
                    <?php if (!empty($errors['visible'])) : ?>
                        <div class="form-text error text-danger">
                            <?= $errors['visible'] ?>
                        </div>
                    <?php endif; ?>
                </div>

                <!-- Розмір -->
                <div class="mb-3 col text-center">
                    <label for="size" class="form-label mb-1">Розмір</label>
                    <select class="form-control" id="size" name="size">
                        <option selected value="0">no-size</option>
                        <option value="17.5">17.5</option>
                        <option value="18">18</option>
                        <option value="18.5">18.5</option>
                        <option value="17">19</option>
                        <option value="17">20</option>
                    </select>
                    <?php if (!empty($errors['size'])) : ?>
                        <div class="form-text error text-danger">
                            <?= $errors['size '] ?>
                        </div>
                    <?php endif; ?>
                </div>

                <!-- зображення для товару -->
                <div class="mb-4 col text-center">
                    <label for="file" class="form-label mb-1">Оберіть файл з зображенням товару</label>
                    <input multiple class="form-control form-control-lg" id="file" name="file" type="file"
                           accept="image/png">
                </div>

                <!-- btn -->
                <div class="mb-3 col text-center">
                    <button class="btn btn-secondary btn-lg text-white text-decoration-none">Додати товар</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://cdn.ckeditor.com/ckeditor5/35.4.0/classic/ckeditor.js"></script>
<script>
    let editors = document.querySelectorAll('.ckeditor');
    for (let editor of editors) {
        ClassicEditor
            .create(editor)
            .catch(error => {
                console.error(error);
            });
    }
</script>

