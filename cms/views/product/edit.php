<?php
/** @var array $product */
/** @var array $categories */

/** @var array $model */
/** @var array $errors */
?>
<div class="container">
    <div class="novelty w-100 h-25 text-center p-3">
        <h2>Редагування продукту</h2>
    </div>
    <div class="row row-cols-10 row-cols-md-3 mx-1 justify-content-around">
        <div class="col text-center mb-3">
            <form action="" method="post" enctype="multipart/form-data">
                <!-- Назва товару -->
                <div class="mb-3">
                    <label for="name" class="form-label">Назва продукту</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Введіть назву товару.."
                           value="<?= $product['name'] ?>">
                    <?php if (!empty($errors['name'])) : ?>
                        <div class="form-text error mb-4 text-danger">
                            <?= $errors['name'] ?>
                        </div>
                    <?php endif; ?>
                </div>
                <!-- Категорії -->
                <div class="mb-3 col text-center">
                    <label for="category_id" class="form-label mb-1">Оберіть категорію товару</label>
                    <select type="text" class="form-control" id="category_id" name="category_id">
                        <? foreach ($categories as $category): ?>
                            <option value="<?= $category['id'] ?>"><?= $category['name'] ?></option>
                        <? endforeach; ?>
                    </select>
                    <?php if (!empty($errors['category_id'])) : ?>
                        <div class="form-text error text-danger">
                            <?= $errors['category_id'] ?>
                        </div>
                    <?php endif; ?>
                </div>
                <!-- Ціна -->
                <div class="mb-3 col text-center">
                    <label for="price" class="form-label mb-1">Ціна товару</label>
                    <input type="number" class="form-control" id="price" name="price"
                           placeholder="Введіть ціну..">
                    <?php if (!empty($errors['price'])) : ?>
                        <div class="form-text error text-danger">
                            <?= $errors['price'] ?>
                        </div>
                    <?php endif; ?>
                </div>

                <!-- Кількість -->
                <div class="mb-3 col text-center">
                    <label for="count" class="form-label mb-1">Кількість одиниць товару</label>
                    <input type="number" class="form-control" id="count" name="count"
                           placeholder="Введіть кількість товарів..">
                    <?php if (!empty($errors['count'])) : ?>
                        <div class="form-text error text-danger">
                            <?= $errors['count'] ?>
                        </div>
                    <?php endif; ?>
                </div>

                <!-- Короткий опис товару -->
                <div class="mb-3 col text-center">
                    <label for="short_description" class="form-label mb-1">Короткий опис товару</label>
                    <textarea type="text" class="ckeditor form-control" id="short_description" name="short_description"
                              placeholder="Введіть короткий опис..">
                    </textarea>
                    <?php if (!empty($errors['short_description'])) : ?>
                        <div class="form-text error text-danger">
                            <?= $errors['short_description'] ?>
                        </div>
                    <?php endif; ?>
                </div>

                <!-- розширений опис товару -->
                <div class="mb-3 col text-center">
                    <label for="description" class="form-label mb-1">Опис товару</label>
                    <textarea class="form-control ckeditor" id="description" name="description"
                              placeholder="Введіть опис.."
                              type="text"></textarea>
                    <?php if (empty($errors['description'])) : ?>
                        <div class="form-text error text-danger">
                            <?= $errors['description'] ?>
                        </div>
                    <?php endif; ?>
                </div>

                <!-- зображення для товару -->
                <div class="mb-3">
                    <label for="file" class="form-label">Оберіть нове зображення для категорії</label>
                    <div class="card mb-2">
                        <?php $filePath = 'files/product/' . $product['photo']; ?>
                        <?php if (is_file($filePath)): ?>
                            <img src="/<?= $filePath ?>" class="card-img-top row-cols-lg-4" alt="category-photo"
                                 height="300">
                        <?php else : ?>
                            <img src="/static/images/default-image.jpg" class="card-img-top row-cols-lg-4"
                                 alt="category-default-photo" height="300">
                        <?php endif; ?>
                    </div>
                    <input class="form-control form-control-lg" id="file" name="file" type="file"
                           accept="image/png">
                </div>

                <div class="mb-3">
                    <button class="btn btn-secondary btn-lg text-white text-decoration-none">Зберегти</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://cdn.ckeditor.com/ckeditor5/35.4.0/classic/ckeditor.js"></script>
<script>
    let editors = document.querySelectorAll('.ckeditor');
    for (let editor of editors) {
        ClassicEditor
            .create(editor)
            .catch(error => {
                console.error(error);
            });
    }
</script>
