<?php
/** @var array $rows */
core\Core::getInstance()->pageParams['title'] = 'Товари';
use models\User;
?>

<div class="row justify-content-around w-100 align-items-center mb-4">
    <div class="row justify-content-around w-100 align-items-center mb-4" style="background: #f9f9fa">
        <h2 class="col-4 my-lg-2 text-center">Список товарів</h2>
        <?php if (User::isAdmin()) : ?>
            <a href="/product/add" class="btn btn-primary mb-4 col-2 mt-4">Додати товар</a>
        <?php endif; ?>
    </div>

    <div class="row justify-content-around w-100 align-items-center mb-4">
        <div class="container">
            <div class="row row-cols-1 row-cols-md-5 g-10 mx-1 categories-list">
                <?php  foreach ($rows as $product) : ?>
                        <div class="col text-center mb-3">
                            <a href="/product/view/<?= $product['id'] ?>" class="card-link">
                                <div class="card" style="min-height: 100%;">
                                    <?php $filePath = 'files/product/' . $product['photo']; ?>
                                    <?php if (is_file($filePath)): ?>
                                        <img src="/<?= $filePath ?>" class="card-img-top row-cols-lg-4" alt="product-photo"
                                             height="250">
                                    <?php else : ?>
                                        <img src="cms/static/images/default-image.jpg" class="card-img-top row-cols-lg-4"
                                             alt="category-default-photo" height="250">
                                    <?php endif; ?>
                                    <div class="card-body" style="text-align: left">
                                        <div class="card-description">
                                            <h5>
                                                <p class="card-description__name"><?= $product['name'];?></p>
                                                <p class="card-description__price"><?= $product['price']?> грн.</p>
                                            </h5>
                                            <div class="review">
                                                <i class="bi bi-bag">
                                                    <svg width="24" height="24" fill="currentColor" class="bi bi-bag" viewBox="0 0 16 17">
                                                        <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z"/>
                                                    </svg>
                                                </i>
                                                <span style="text-transform: uppercase; border-bottom: #c5811c 1px solid"> Переглянути</span>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if (User::isAdmin()) : ?>
                                    <div class="card-body mt-1 w-100">
                                        <a href="/product/edit/<?= $product['id'] ?>" class="card-link btn btn-primary text-light mb-1">Редагувати</a>
                                        <a href="/product/delete/<?= $product['id'] ?>" class="card-link btn btn-danger text-light">Видалити</a>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
            </div>

        </div>
    </div>


</div>
