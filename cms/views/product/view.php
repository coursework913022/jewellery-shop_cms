<?php
/** @var array $category_product */
?>

<div class="container my-3">
    <div class="card mh-100">
        <div class="row g-0">
            <h2 class="card-title col-md-12 text-center border py-3 m-0">
                <?= $category_product['name'] ?>
                <a href="http://cms/category/view/<?=$category_product['category_id']?>" class="card-link">Back</a>
            </h2>
            <div class="col-md-4">
                <?php $filePath = 'files/product/' . $category_product['photo']; ?>
                <?php if (is_file($filePath)): ?>
                    <img src="/<?= $filePath ?>" class="img-fluid rounded-start" alt="product-photo">
                <?php else : ?>
                    <img src="/static/images/default-image.jpg" class="img-fluid rounded-start h-100 w-100"
                         alt="category-default-photo">
                <?php endif; ?>
            </div>
            <div class="col-md-8" style="font-size: 1.2rem">
                <div class="card-body mx-2 mt-2">


                    <? if (!empty($category_product['short_description'])) : ?>
                        <div class="short-description h2 pb-5">
                            <?= $category_product['short_description'] ?>
                        </div>
                    <? endif; ?>

                    <!--price-->
                    <div class="container row mt-2">
                        <div class="col-4">
                            <label for="count">Ціна товару: </label>
                        </div>
                        <div class="col-8">
                            <b><?= $category_product['price'] ?>грн.</b>
                        </div>
                    </div>

                    <!--size-->
                    <? if ($category_product['size'] > 10 && $category_product['size'] < 25) : ?>
                        <div class="container row mt-2">
                            <div class="col-4">
                                <label for="count">Розмір товару: </label>
                            </div>
                            <div class="col-8">
                                <b><?= $category_product['size'] ?></b>
                            </div>
                        </div>
                    <? endif; ?>

                    <!-- count -->
                    <div class="container row mt-2">
                        <div class="col-4">
                            <label for="count">Доступна кількість: </label>
                        </div>
                        <div class="col-8">
                            <strong><?= $category_product['count'] ?> шт.</strong>
                        </div>
                    </div>


                    <!--buy-->
                    <div class="container row mt-4">
                        <div class="col-4">
                            <label for="count">Кількість товарних одиниць до замовлення: </label>
                        </div>
                        <div class="col-2">
                            <form action="" method="post">
                                <input class="form-control" type="number" name="count" id="count" min="1" value="1"
                                       max="<?= $category_product['count'] ?>">
                                <button class="btn btn-primary mt-3 px-3">Придбати</button>
                                <input type="hidden" name="product-id" value="<?= $category_product['id'] ?>">
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!--description-->
            <? if (!empty($category_product['description'])) : ?>
                <div class="container row">
                    <div class="col-12">
                        <div class="description mt-5" style="font-size: 1.2rem"><?= $category_product['description'] ?></div>
                    </div>
                </div>
            <? endif; ?>
        </div>
    </div>
</div>

