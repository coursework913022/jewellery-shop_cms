<?php
/** @var array $product */
?>
<div class="container">
    <div class="row row-cols-10 row-cols-md-2 mx-1 justify-content-around align-items-center">
        <div class="col text-center mb-3">
            <div class="alert alert-danger mt-3  h-100" role="alert">
                <h4 class="alert-heading h2 fw-bold text-dark">Видалити товар "<?= $product['name'] ?>"?</h4>
                <p class="text-dark">Після видалення, товар зникне з бази даних </p>
                <hr>
                <div class="mb-3">
                    <a href="/product/delete/<?= $product['id'] ?>/yes"
                       class="btn btn-danger btn-lg text-white text-decoration-none mb-2">Видалити</a>
                    <a href="/product"
                       class="btn btn-success btn-lg text-white text-decoration-none mb-2">Відмінити</a>
                </div>
            </div>
        </div>
    </div>
</div>


