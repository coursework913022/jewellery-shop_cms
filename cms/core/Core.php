<?php

namespace core;

use controllers\MainController;

class Core
{
    private static $instance = null;
    public $app;
    public $pageParams;
    public DB $db;
    public $requestMethod;

    private function __construct()
    {
        global $pageParams;
        $this->app = [];
        $this->pageParams = $pageParams;
    }

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    #For FrontController

    //зєднання з базоб даних, сесією.
    public function Initialize()
    {
        session_start();
        $this->db = new DB(DATABASE_HOST, DATABASE_LOGIN,
            DATABASE_PASSWORD, DATABASE_BASENAME);
        $this->requestMethod = $_SERVER['REQUEST_METHOD'];
    }

    // маршрутизація, логіка по виклику методів контролера, повернення з них результату.
    public function Run()
    {
        $route = $_GET['route'];
        $routeParts = explode('/', $route);

        $moduleName = strtolower(array_shift($routeParts));
        if (empty($moduleName)) {
            $moduleName = "main";
        }
        $actionName = strtolower(array_shift($routeParts));
        if (empty($actionName)) {
            $actionName = "index";
        }

        $this->app['moduleName'] = $moduleName;
        $this->app['actionName'] = $actionName;

        $controllerName = '\\controllers\\'.ucfirst($moduleName).'Controller';
        $controllerActionName = $actionName.'Action';

        $statusCode = 200;

        if (class_exists($controllerName)) {
            $controller = new $controllerName(); //екземпляр контролеру(передаємо назву класу $controllerName())

            if (method_exists($controller, $controllerActionName)) {
                $actionResult = $controller->$controllerActionName($routeParts);
                if($actionResult instanceof Error){
                    $statusCode = $actionResult->code;
                }
                $this->pageParams['content'] = $actionResult;
            } else {
                $statusCode = 404;
            }
        } else {
            $statusCode = 404;
        }
        $statusCodeType = intval($statusCode / 100);
        if ($statusCodeType == 4 || $statusCodeType == 5) {
            $mainController = new MainController();
            $this->pageParams['content'] = $mainController->errorAction($statusCode);
        }
    }

    // Відображати вже сформовану сторінку.
    public function Done()
    {
        $pathToLayout = 'themes/light/layout.php';
        $tpl = new Template($pathToLayout);
        $tpl->setParams($this->pageParams);
        $html = $tpl->getHTML();
        echo $html;
    }
    #/For FrontController
}