<?php

namespace core;

/**клас шаблонізатора який дозволить взяти заданий файл, проінклудить його,
 * а результат поверне як рядок, без виводу на екран*/
class Template
{
    protected $path;
    protected $params;

    public function __construct($path)
    {
        $this->path = $path;
        $this->params = [];
    }

    public function setParam($name, $value)
    {
        $this->params[$name] = $value;
    }

    public function setParams($params)
    {
        foreach ($params as $name => $value) {
            $this->setParam($name, $value);
        }
    }

    public function getHTML()
    {
        ob_start(); #активуэ буфер того що ми відправляємо в браузер(клієнту)
        extract($this->params);

        include($this->path);
        $html = ob_get_contents(); #повертає вміст буферу
        ob_end_clean(); #завершення збору інформації, очищення буфера
        return $html;
    }
}