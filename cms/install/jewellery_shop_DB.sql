-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Січ 14 2023 р., 13:13
-- Версія сервера: 8.0.30
-- Версія PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `jewellery_shop_DB`
--

-- --------------------------------------------------------

--
-- Структура таблиці `category`
--

CREATE TABLE `category` (
  `id` int NOT NULL COMMENT 'ID',
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'Назва котегорії'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Дамп даних таблиці `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Браслети'),
(3, 'Каблучки'),
(4, 'Сережки'),
(5, 'Кольє'),
(6, 'Підвіски');

-- --------------------------------------------------------

--
-- Структура таблиці `materials`
--

CREATE TABLE `materials` (
  `id` int NOT NULL COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'Назва виробника'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Дамп даних таблиці `materials`
--

INSERT INTO `materials` (`id`, `name`) VALUES
(1, 'Золото'),
(2, 'Срібло');

-- --------------------------------------------------------

--
-- Структура таблиці `product`
--

CREATE TABLE `product` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL COMMENT 'Назва товару',
  `material_id` int NOT NULL COMMENT 'Матеріал',
  `category_id` int NOT NULL COMMENT 'Категорія товару',
  `price` float NOT NULL COMMENT 'Ціна',
  `count` int NOT NULL COMMENT 'К-ть',
  `short_text` text COLLATE utf8mb3_unicode_ci COMMENT 'Короткий опис товару',
  `min_size` int DEFAULT NULL COMMENT 'Мінімальний розмір',
  `max_size` int DEFAULT NULL COMMENT 'Максимальний розмір'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Дамп даних таблиці `product`
--

INSERT INTO `product` (`id`, `name`, `material_id`, `category_id`, `price`, `count`, `short_text`, `min_size`, `max_size`) VALUES
(1, 'Срібний браслет \"Кульки\" з чорними фіанітами', 2, 1, 1444, 100, 'Срібний браслет \"Кульки\" виконаний зі срібла 925 проби та вставками камінців  фіаніту. Довжину браслету можна редагувати.', 17, 20),
(2, 'Сережки в сріблі з штучними перлами 122758', 2, 4, 1550, 70, 'Стильні сережки зі срібла 925 проби з штучними перлами, без покриття. Вага 4.7 г.', NULL, NULL),
(3, 'Золота каблучка \"Вишиванка\" з чорнінням', 1, 3, 15160, 100, 'Каблучка \"Вишиванка\" виконана з червоного золота 585 проби. Без каменів. Місцямии покрита чорнінням. Вага 5.83 г.', 14, 22);

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `materials`
--
ALTER TABLE `materials`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `producer_id` (`material_id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `category`
--
ALTER TABLE `category`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблиці `materials`
--
ALTER TABLE `materials`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблиці `product`
--
ALTER TABLE `product`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`material_id`) REFERENCES `materials` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
